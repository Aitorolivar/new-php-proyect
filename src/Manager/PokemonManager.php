<?php

namespace App\Manager;

use Mael\InterventionImageBundle\MaelInterventionImageManager;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class PokemonManager
{
    protected $imageManager;
    protected $watermark;
    protected $mailer;

    public function __construct(MaelInterventionImageManager $imageManager, $waterMark, MailerInterface $mailer)
    {
        $this->imageManager = $imageManager;
        $this->watermark = $waterMark;
        $this->mailer = $mailer;
    }

    public function updateImage($image)
    {
        $this->imageManager->make($image)->insert($this->watermark)->text("A ver que sale")->greyscale()->save($image);
    }

    public function sendEmail()
    {
        $email = (new Email())
            ->from('moycarretero@gmail.com')
            ->to('morato.delacalle@bootcamp-upgrade.com')
            //->cc('cc@example.com')
            //->bcc('bcc@example.com')
            //->replyTo('fabien@example.com')
            //->priority(Email::PRIORITY_HIGH)
            ->subject('He creado un Pokemon!!!!')
            ->text('Esto es un email!')
            ->html('<p>See Twig integration for better HTML integration!</p>');

        $this->mailer->send($email);

        // ...
    }
}