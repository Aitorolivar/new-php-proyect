<?php

namespace App\Controller;

use App\Entity\Debilidades;
use App\Entity\Pokemon;
use App\Form\PokemonFormType;
use App\Manager\PokemonManager;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

class PokemonController extends AbstractController
{
    /**
     * @Route("/pokemon/{id}", name="getPokemon")
     */
    public function getPokemon(Pokemon $pokemon)
    {
        return $this->render("pokemons/showPokemon.html.twig", ["pokemon" => $pokemon]);
    }
    /*public function getPokemon($id, EntityManagerInterface $doctrine)
    {
        $repo = $doctrine->getRepository(Pokemon::class);
        $pokemon=$repo->find($id);
        return $this-> render("pokemons/showPokemon.html.twig",["pokemon" => $pokemon]);
    }*/

    /**
     * @Route ("/pokemons", name="listPokemons")
     */
    public function listPokemons(EntityManagerInterface $doctrine)
    {
        $repo = $doctrine->getRepository(Pokemon::class);
        $pokemons = $repo->findAll();
        return $this->render("pokemons/listPokemons.html.twig", ["pokemons" => $pokemons]);
    }

    /**
     * @Route("/createPokemon", name="createPokemon")
     */
    public function createPokemon(Request $request, EntityManagerInterface $doctrine, SluggerInterface $slugger, PokemonManager $pm)
    {
        $form = $this->createForm(PokemonFormType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $image = $form->get('photo')->getData();
            $pokemon = $form->getData();

            $path = $this->getParameter('kernel.project_dir').'/public/img';
            $filename = $slugger->slug($pokemon->getName()).'.'.$image->guessClientExtension();
            $image->move($path, $filename);

            $pokemon->setImage("/img/$filename");

            $pm->updateImage("$path/$filename");

            $doctrine->persist($pokemon);
            $doctrine->flush();
            $this->addFlash('success', 'Pokemon creado correctamente');

            $pm->sendEmail();

            return $this->redirectToRoute('listPokemons');
        }

        return $this->render('pokemons/createPokemon.html.twig', ['pokemonsForm' => $form->createView()]);
    }

    /**
     * @Route("/updatePokemon/{id}", name="updatePokemon")
     */
    public function updatePokemon(Pokemon $pokemon, Request $request, EntityManagerInterface $doctrine)
    {
        $form = $this->createForm(PokemonFormType::class, $pokemon);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $pokemon = $form->getData();
            $doctrine->persist($pokemon);
            $doctrine->flush();
            $this->addFlash('success', 'Pokemon creado correctamente');
            return $this->redirectToRoute('listPokemons');
        }

        return $this->render('pokemons/createPokemon.html.twig', ['pokemonsForm' => $form->createView()]);
    }


    /**
     * @Route("/deltepokemon/{id}", name="deletePokemon")
     */
    public function deletePokemon(Pokemon $pokemon, EntityManagerInterface $doctrine)
    {
        $doctrine->remove($pokemon);
        $doctrine->flush();

        return $this->redirectToRoute("listPokemons");
    }

    /**
     * @Route("/newDebilidades")
     */

    public function newDebilidades(EntityManagerInterface $doctrine)
    {
        $debilidades = new Debilidades();
        $debilidades->setName('Tierra');
        $doctrine->persist($debilidades);

        $debilidades2 = new Debilidades();
        $debilidades2->setName('Agua');
        $doctrine->persist($debilidades2);

        $debilidades3 = new Debilidades();
        $debilidades3->setName('Fuego');
        $doctrine->persist($debilidades3);
        $doctrine->flush();
    }

    /**
     * @Route("/newpokemons")
     */
    public function insertPokemon(EntityManagerInterface $doctrine)
    {

        $repo = $doctrine->getRepository(Debilidades::class);
        $fuego = $repo->find(3);
        $tierra = $repo->find(1);
        $agua = $repo->find(2);

        $pokemon = new Pokemon();
        $pokemon->setName('Emboar');
        $pokemon->setDescription('Calienta sus puños con las llamas de su barbilla para propinar puñetazos ardientes. Es muy fiel a sus compañeros. ');
        $pokemon->setCode(500);
        $pokemon->setImage("https://assets.pokemon.com/assets/cms2/img/pokedex/full/500.png");
        $pokemon->addDebilidade($fuego);


        $doctrine->persist($pokemon);

        $pokemon2 = new Pokemon();
        $pokemon2->setName('Minun');
        $pokemon2->setDescription('Minun se preocupa más por alentar a sus compañeros de equipo que por su propia seguridad. Para animar a los miembros de su grupo, crea un cortocircuito en su interior y libera un chisporroteo espectacular. ');
        $pokemon2->setCode(312);
        $pokemon2->setImage("https://assets.pokemon.com/assets/cms2/img/pokedex/full/312.png");
        $pokemon2->addDebilidade($tierra);
        $pokemon2->addDebilidade($agua);


        $doctrine->persist($pokemon2);

        $doctrine->flush();

        $pokemon->setCode(600);

        $doctrine->flush();

        return $this->render("base.html.twig");
    }

    /**
     * @Route("/admin/home")
     */
    public function adminPage()
    {
        return new Response("Estoy en una página de Administarción");
    }
}
