<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    public function hola()
    {
        return new Response("Hola Mundo");
    }

    /**
     * @Route("/user/{nombre}/edad/{edad}", name="usuario", methods={"GET", "POST"})
     */
    public function usuario($nombre, $edad)
    {
        $user = ['name'=>$nombre, 'age'=>$edad];
        return $this->render("user/user.html.twig", ["user" => $user]);
    }

    /**
     * @Route ("/calcula/{numero}", requirements={"numero"="\d+"})
     */
    public function calculaNumero($numero)
    {
        return new Response("Esto es un numero");
    }

    /**
     * @Route ("/user/{id}")
     */
    public function calculaTexto($id)
    {
        $user = [
            'nombre' => 'Moises',
            'apellidos' => 'Carretero Lavado',
            'estudios' =>
                [
                    [
                        'carretera' => 'I.I.'
                    ],
                    [
                        'carrerar' => 'Doctorado'
                    ]
                ],
            'edad' => 33
        ];

        return new JsonResponse($user);
    }




}