<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211129142127 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE debilidades (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE debilidades_pokemon (debilidades_id INT NOT NULL, pokemon_id INT NOT NULL, INDEX IDX_2D98C42ED8A0D6A2 (debilidades_id), INDEX IDX_2D98C42E2FE71C3E (pokemon_id), PRIMARY KEY(debilidades_id, pokemon_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE debilidades_pokemon ADD CONSTRAINT FK_2D98C42ED8A0D6A2 FOREIGN KEY (debilidades_id) REFERENCES debilidades (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE debilidades_pokemon ADD CONSTRAINT FK_2D98C42E2FE71C3E FOREIGN KEY (pokemon_id) REFERENCES pokemon (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE debilidades_pokemon DROP FOREIGN KEY FK_2D98C42ED8A0D6A2');
        $this->addSql('DROP TABLE debilidades');
        $this->addSql('DROP TABLE debilidades_pokemon');
    }
}
